*** Settings ***
Library     SeleniumLibrary
Library     AutoItLibrary
*** Test Cases ***

TC1 Windows credentials with url
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    https://admin:admin@the-internet.herokuapp.com/basic_auth
    Sleep    5s
    [Teardown]  Close Browser

TC2 Windows credentials with autoIt
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    https://the-internet.herokuapp.com/basic_auth
    AutoItLibrary.send  admin
    AutoItLibrary.send  {TAB}
    AutoItLibrary.send  admin
    AutoItLibrary.send  {ENTER}
    Sleep    5s
    [Teardown]  Close Browser

TC3 Windows upload
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    https://www.ilovepdf.com/pdf_to_word
    Click Element    id:pickfiles
    Sleep    5s
    Control Focus   Open    ${EMPTY}    Edit1
    Control Set Text    Open    ${EMPTY}    Edit1   C:${/}Automation Concepts${/}demofile.pdf
    Control Click   Open    ${EMPTY}    Button1
    Sleep    5s
    [Teardown]  Close Browser
