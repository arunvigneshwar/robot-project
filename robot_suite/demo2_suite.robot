*** Settings ***
Library    String
*** Test Cases ***
TC1
  ${name}  Set Variable  robot framework
  ${result}  Convert To Upper Case  ${name}
  Log To Console     ${result}
  
TC2
  ${num1}  Set Variable  $102,000

  ${num2}  Set Variable  $201,500,000
  ${num1}  Remove String    ${num1}  $  ,
  ${num2}  Remove String    ${num2}  $  ,
  ${result}  Evaluate  ${num1} + ${num2}
  Log To Console    ${result}