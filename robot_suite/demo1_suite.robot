*** Settings ***
Library    DateTime
*** Test Cases ***
TC1
  Log To Console    message=Arun Vigneshwar
TC2
    ${radius}  Set Variable  10
    ${result}  Evaluate  3.14*${radius}*${radius}
    Log To Console  Area of Circle: ${result}

TC3
  ${current_date}  Get Current Date
  Log To Console  ${current_date}
