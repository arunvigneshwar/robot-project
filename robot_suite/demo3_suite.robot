*** Settings ***
Library    Collections

*** Variables ***
${BROWSER_NAME}  chrome
@{colors}  red  blue  green
&{MY_DETAILS}  name=Arun  mobile=9453
*** Test Cases ***
TC1
  @{fruits}  Create List  apple  mango  banana
  ${size}  Get Length    ${fruits}
  Log To Console    ${size}
  Append To List  ${fruits}  grape
  Remove Values From List    ${fruits}  apple
  Insert Into List    ${fruits}    0  jackfruit
  Log To Console  ${fruits}
  