*** Settings ***
Library     RequestsLibrary


*** Test Cases ***
TC1 Find Valid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/5       expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
    Status Should Be    200
    Log    ${response.json()}[id]
    Should Be Equal As Integers    ${response.json()}[id]    5

TC2 Find by valid status
     Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/findByStatus?status=sold      expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.json()}[0][status]
    Should Be Equal As Strings    ${response.json()}[0][status]    sold
    FOR    ${element}    IN    @{response.json()}
        Log    ${element}
        Should Be Equal As Strings    ${element}[status]   sold
    END

TC1 Test invalid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session      alias=petshop   url=pet/5000       expected_status=404
    Log    ${response}
    Status Should Be    404
    Log To Console    ${response.json()}[message]
    Should Be Equal As Strings    ${response.json()}[message]    Pet not found