*** Settings ***
Library     RequestsLibrary
Library     OperatingSystem

*** Test Cases ***
TC1
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    &{header-dic}   Create Dictionary    Content-Type=application/json  Connection=keep-alive
    &{json}     Get Binary File    ${EXECDIR}${/}test_data${/}new_pet.json
    ${response}     POST On Session     alias=petshop   url=pet/6501    headers=${header-dic}   data=&{json}
    Log To Console    ${response}