*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://netbanking.hdfcbank.com/netbanking/IpinResetUsingOTP.htm
    Click Element    xpath=//img[@alt='Go']
    ${actual_alert}  Handle Alert  action=ACCEPT  timeout=20s
    Should Be Equal    ${actual_alert}    Customer ID${SPACE}${SPACE}cannot be left blank.
    