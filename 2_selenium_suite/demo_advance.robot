*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Choose File    xpath=//input[@type='file']    C:${/}Automation Concepts${/}demofile.pdf

TC3 Nasscom
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://nasscom.in/about-us/contact-us
    Mouse Over    xpath=//a[text()='Membership']
    #mouse over on Become a member
    Mouse Over    xpath=//a[text()='Become a Member']
    Click Element    xpath=//a[text()='Membership Benefits']
TC4 Php Javascript
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net/

    Execute Javascript  document.querySelector('#checkin').value='19-08-2023';
    Execute Javascript  document.querySelector('#checkout').value='26-08-2023'

    Execute Javascript  document.querySelector('#adults').value=4
    Execute Javascript  document.querySelector('#childs').value=2

    #checkin 19-08-2023
    #checkout 26-08-2023