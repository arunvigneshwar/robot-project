*** Settings ***
Library    SeleniumLibrary
*** Test Cases ***
TC1
    Open Browser    url=https://github.com/login    browser=chrome
    Input Text    id:login_field    hello
    Input Password    id:password    89hello
    Click Element    name:commit
    Element Text Should Be    xpath=//div[contains(@class,'flash-error')]  Incorrect username or password.
TC2
    Open Browser  browser=chrome
    Maximize Browser Window
    Go To    url=https://www.salesforce.com/in/form/signup/freetrial-sales/
    Input Text    name:UserFirstName    john
    Input Text    name:UserLastName    wick
    Input Text    name:UserEmail  john@gmail.com
    Select From List By Label    name:UserTitle  IT Manager
    Select From List By Label    name:CompanyEmployees  201 - 500 employees
    Select From List By Label    name:CompanyCountry  United Kingdom
    Click Element    xpath=//div[@class= 'checkbox-ui']
    Click Element    xpath=//button[@name= 'start my free trial']
    Element Text Should Be    xpath=//span[contains(@id,'CompanyName')]  Enter your company name
    Element Text Should Be    xpath=//span[contains(@id,'UserPhone')]  Enter a valid phone number