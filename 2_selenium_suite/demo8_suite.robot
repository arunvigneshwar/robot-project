*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}  Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}
        IF    '${name}' == 'Thor'
            Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
            Exit For Loop
        END
    END