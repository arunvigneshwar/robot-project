*** Settings ***
Library    SeleniumLibrary
*** Test Cases ***
TC1
    Open Browser  browser=chrome
    Maximize Browser Window
    Go To    url=https://www.db4free.net/
    Click Element    partial link=phpMyAdmin
    Switch Window  phpMyAdmin
    Input Text    id:input_username    admin
    Input Password    id:input_password    welcome123
    Click Element    id:input_go
    Element Should Contain    id:pma_errors    Access denied for user
    #Element Should Contain    xpath=//div[@role='alert'][3]    expected

TC3
    Open Browser  browser=chrome
    Maximize Browser Window
    Go To    url=https://www.online.citibank.co.in
    Set Selenium Implicit Wait    20s
    ${ele_count}  Get Element Count    xpath=//a[@class='newclose']
#    IF  ${ele_count}>0
#      Click Element    xpath=//a[@class='newclose'
#    END
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose']
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose2']
    Click Element    xpath=//span[text()='Login']
    Switch Window  NEW
    Click Element    xpath=//div[contains(text(),'Forgot User')]
    Click Element    link=select your product type
    Click Element    link=Credit Card