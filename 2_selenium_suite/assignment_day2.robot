*** Settings ***
Library    SeleniumLibrary
*** Test Cases ***
TC1
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=http://demo.openemr.io/b/openemr/interface/login/login.php?site=default
    Input Text    id:authUser    admin
    Input Password    id:clearPass    pass
    Select From List By Label    name:languageChoice  English (Indian)
    Click Element    xpath=//button[@id='login-button']
    Mouse Over    xpath=//div[@role='button'][1]
    Click Element    xpath=//div[contains(text(),'New/Search')]
    Select Frame    xpath://iframe[@name='pat']
    Input Text    id:form_fname    test1
    Input Text    id:form_lname    test2
    Input Text    id:form_DOB    2023-05-23
    Select From List By Label    id:form_sex  Male
    Click Element    id:create
    Unselect Frame
    Select Frame    id:modalframe
    Click Element    xpath=//button[normalize-space()='Confirm Create New Patient']

TC2

    