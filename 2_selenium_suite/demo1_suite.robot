*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser  url=https://www.facebook.com  browser=chrome
    ${title}  Get Title
    Log To Console    ${title}
TC2
    Open Browser  url=https://www.facebook.com  browser=chrome
    Input Text    id:email    Arunnnnn
    Input Password    id:pass    password
    Click Element    name:login
    Close Browser
TC3
    Open Browser  url=https://www.facebook.com  browser=chrome
    Click Element  link=Create new account
    Set Selenium Implicit Wait    30s
    #Wait Until Page Contains Element    name:firstname
    Input Text    name:firstname    john
    Input Text    name:lastname    wick
    Input Password    name:reg_passwd__    welcome123
    Select Radio Button    sex    -1
    Select From List By Label    id:day  5
    Select From List By Label    id:month  Nov
    Select From List By Label    id:year  1992
    Close Browser